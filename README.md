## Getting started

```bash
./build.sh    # builds docker image
./run.sh      # runs docker image with X11
./compile.sh  # compiles and runs an opengl app
```
