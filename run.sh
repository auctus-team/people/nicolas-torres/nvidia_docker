#!/usr/bin/env bash
set -e

mypwd=$(pwd)

docker run --rm -it \
       --gpus all --runtime=nvidia \
       --volume ${mypwd}:${mypwd} \
       --volume ${XAUTHORITY}:${XAUTHORITY}:ro \
       --volume /tmp/.X11-unix:/tmp/.X11-unix \
       --workdir=${mypwd} \
       --network=host \
       -e DISPLAY -e XAUTHORITY \
       --env=QT_X11_NO_MITSHM=1 \
       --env=TZ=Europe/Paris \
       --security-opt=apparmor:unconfined \
       nvidia_opensim
