#!/usr/bin/env bash
set -e

shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'
alias say='{ set +x; } 2>/dev/null; echo $1'

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

echo "- SCRIPT_DIR: $SCRIPT_DIR"

# get image tag from root basename (upper from script)
image_tag=nvidia_opensim
echo "- image tag: ${image_tag}"

echo "- build image"
trace_on
docker build -t ${image_tag} .
trace_off
